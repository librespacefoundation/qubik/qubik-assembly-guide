.. QUBIK Assembly Guide documentation master file

.. |TQWrench| image:: img/torque-wrench-45.png
   :height: 50px
   :alt: alternate text

.. |TopSolarScrews| replace:: M2x6, DIN7985
.. |BasePlateNuts| replace:: M2.5 DIN934
.. |FrameScrews| replace:: M2.5x60 DIN965

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: Contents:

QUBIK Assembly Guide
=====================
Please read through this guide before proceeding

Glove box preparation
=====================
* Check anti-static mat is connected to ground
* Check anti-static bracelets are connected to ground
* Check glove box is clean from dust, if not use vacuum to clean it
* Wear anti-static bracelets during the whole process
* Wear gloves during the whole process
* Wear properly the sleeves of glove box
* In all process the glove box must be remained clean

Tools
=====
*Clean and insert all needed tools in the glove box:*

* metal self-locking tweezers
* metal curved tweezers
* torque-wrench with 5mm socket(needed for M2 and M2.5 PH screws and nuts)
* scissors
* M2.5 nut tightener bit for 5mm socket
* 5mm hex key
* thrash can
* PH-1 screw driver
* PH-1 bit for 5mm socket
* cutting pliers (flush)
* Epoxy Resin Adhesive Dispenser
* Poke needle for applying epoxy glue

Consumables
===========
*Clean and insert all needed consumables in the glove box:*

* Dyneema string, 0.14mm diameter
* Loctite 9466 - Epoxy glue
* Epoxy mixing nozzle tip (for epoxy dispenser)
* Blue threadlocker glue
* Cyanoacrylate glue

Parts
=====
*Assume that all the parts are ready to fly (tested, glued and conformal coating, cleaned).*

* RBF pin
* Assembly stand
* Assembled Base plate
* Top Frame
* 4x [FrameScrews], long screws
* 4x battery stand-offs, long stand-offs
* Assembled BMPS, included battery pack
* 12x short stand-offs
* Assembled ballast board
* Assembled PQ9ish COMMS
* Bottom Frame
* COMMS Programmer cable
* Main harness
* Antenna cable
* 4x [BasePlateNuts] nuts
* 4x Assembled Side solar panels
* Assembled Top solar panel
* 4x [TopSolarScrews]

Assembly Steps
==============

Base Plate
----------
.. container:: step

  .. rubric:: Base Plate to Assembly stand

  .. image:: img/baseplate-stand.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Insert cleaned Base Plate into the "Assembly stand" with thermal knifes first
  * Lock it with the RBF pin

.. container:: step

  .. rubric:: Harness connection

  .. image:: img/baseplate-harness.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Connect QBP2CH harness to connector J2
  * Insert QBP2BMPSH harness to connector J1
  * Insert U.Fl Antenna cable to J6

.. container:: step

  .. rubric:: Secure cables with Epoxy

  .. image:: img/noimage.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Prepare epoxy glue
  * Apply a small amount of glue on all connected connectors on the base plate

Top frame
---------

.. container:: step

  .. rubric:: Top Frame

  .. image:: img/frame-top-bolts.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Take one "top frame" and four |FrameScrews| screws
  * insert |FrameScrews| screws from the countersink side of holes
  * Place kapton tape on screw heads as to not fall when frame is handled

.. container:: step

  .. image:: img/frame-top-spacers.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Place the frame with the bolt heads facing down
  * Insert four long stand-offs onto each bolt and set assembly aside

BMPS
----

.. container:: step

  .. rubric:: Connect battery
  .. image:: img/connect-battery.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Connect Battery to BMPS connector J2

.. container:: step

  .. rubric:: Attach BMPS
  .. image:: img/bmps-inserted.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Orient BMPS so PQ9 connector is on the same side as the frame antenna notch of "top frame"
  * Take assembled BMPS and place it on the stack.

  +-------------------------------------------------------------------------------------------+
  | **Important:** Make sure that the harness is on the outside of the frame screw and spacer |
  +-------------------------------------------------------------------------------------------+

Mass Ballast
------------

.. container:: step

  .. rubric:: Attach Mass Ballast
  .. image:: img/ballast-spacers.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Add four short stand-offs

.. container:: step

  .. image:: img/ballast-inserted.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Add assembled ballast board to the stack, align PQ9ISH connector (top/bottom left/right based on connector)

COMMS
-----
.. container:: step

  .. rubric:: Attach COMMS
  .. image:: img/comms-spacers.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Add four short stand-offs

.. container:: step

  .. image:: img/comms-inserted.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Add COMMS board and align with PQ9ish connector
  * Add four short stand-offs

Bottom frame
------------
.. container:: step

  .. rubric:: Attach bottom frame
  .. image:: img/bottom-frame-inserted.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Place "bottom frame" onto the stack
  * Hand-Tighten the M2.5 screws leaving about 5mm at the tip of the screw from bottom frame

  **Note:** The small gap under the bottom frame will ease solar panel installation

Attachment to base Plate
------------------------
.. container:: step

  .. rubric:: Attach structure to base plate
  .. image:: img/base-plate-align.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Align the structure with base plate
  * The PQ9ish connector should on the same side with the thermal knifes (Y-)

.. container:: step

  .. image:: img/bottom-frame-harness.jpg
     :width: 40%
     :alt: alternate text
     :align: left

  * Pass QBP2BMPSH 6 pin connector between bottom frame and COMMS on X+ Side
  * Pass QBP2BMPSH 4 pin connector between bottom frame and COMMS on Y+ Side
  * Pass QBP2CH between bottom frame and COMMS on Y+ Side

.. container:: step

  .. image:: img/noimage.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Connect the antenna connector to COMMS

  **Note:** the antenna connector can be connected later but it will be more difficult to position

.. container:: step

  .. image:: img/noimage.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Place the structure on the base plate taking care that the screws go through the respective screw holes in the base plate
  * Place a rubber band over the entire structure and assembly Frame

  +-----------------------------------------------------------------------------------------+
  | **Attention** Make sure that cables are not pinched between bottom frame and base plate |
  +-----------------------------------------------------------------------------------------+

  * Flip over the entire structure and place it with the top frame resting on the table

.. container:: step

  .. image:: img/qubik-bottom-side-nuts.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Place the 4 [BasePlateNuts] nuts on the screws
  * Hand tighten the nuts until the bottom frame does not move

  +-----------------------------------------------------------------------------------------+
  | **Attention** Make sure that cables are not pinched between bottom frame and base plate |
  +-----------------------------------------------------------------------------------------+

  * Flip the structure over with the assembly base laying flat on the table
  * Remove rubber band

  **Note:** There will be a free play on the top frame and subsystems. This is normal in order to assist in side panel placement

Harness connection
------------------
.. topic:: Preparation:

  | In the following steps epoxy glue will need to be applied.
  | Read carefully the procedure before preparing epoxy glue.

.. container:: step

  .. rubric:: Connect harnesses
  .. image:: img/noimage.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Connect QBP2CH harness to COMMS 6-pin connector
  * Apply epoxy glue

.. container:: step

  .. image:: img/bmps-harness-epoxy.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Connect QBP2BMPSH to BMPS J1
  * Apply epoxy glue

  +--------------------------------------------------------------------------------------------------------+
  | **Attention** At this point, removing the satellite from the assembly base will activate the satellite |
  +--------------------------------------------------------------------------------------------------------+

.. container:: step

  .. image:: img/noimage.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Apply epoxy glue to COMMS antenna cable

  **Tip:** Use a small plastic rod to apply glue

  **Note:** Allow epoxy to dry before continuing to the next steps

Side solar panels
-----------------

**Preface:**
Side solar panels are secured all at once by the top and bottom frames.
This will require some coordination for the correct alignment

Side solar panels have an inductor protruding on the inner side.
This inductor is positioned in such a way that will slide between 2 subsystems

All side solar panels are interchangeable

.. container:: step

  .. rubric:: Attach side solar panels
  .. image:: img/side-panels-exploaded.png
     :width: 40%
     :alt: alternate text
     :align: left

  .. image:: img/side-panel-coil-detail.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Position side solar panels
  * If needed gently lift the subsystems in order to align the solar panel inductor
  * Add a rubber band to keep all side solar panels in place
  * Align top and bottom tabs of the solar panels with the top and bottom frame slots
  * Gently push the top frame towards the base plate until all solar panels are secure

.. container:: step

  .. image:: img/top-frame-bolts-with-panels.png
     :width: 40%
     :alt: alternate text
     :align: left


  * Place a rubber band over the entire structure and assembly base in order for the side solar panels to remain in place
  * Carefully screw the |FrameScrews| screws
  * Remove all rubber bands

  +----------------+------------------+
  | |TQWrench| **Tightening torques** |
  +================+==================+
  | |FrameScrews|  | **0.6 Nm**       |
  +----------------+------------------+

.. container:: step

  .. image:: img/qubik-bottom-side-nuts.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Flip over the entire structure and place it with the top frame resting on the table
  * Screw the 4 |BasePlateNuts| nuts

  +----------------+------------------+
  | |TQWrench| **Tightening torques** |
  +================+==================+
  | **Apply Loctite 9466**            |
  +----------------+------------------+
  | |BasePlateNuts|| **0.6 Nm**       |
  +----------------+------------------+

Top solar panel
---------------

.. container:: step

  .. rubric:: Attach top solar panels
  .. image:: img/top-panel-epoxy.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Connect harness QBP2BMPSH to connector J1 on Top Solar Panel
  * Apply epoxy glue on J1 connector
  * Ensure that the six Top Solar Panel contact pads are covered with Kapton tape

.. container:: step

  .. image:: img/qubik-top-panel-screws.png
     :width: 40%
     :alt: alternate text
     :align: left

  * Align the top solar panel notches with the antenna (Y axis). Harness connector should be on the harness side (Y+)
  * Secure top solar panel using 4 |TopSolarScrews| screws

  +----------------------+------------------+
  | |TQWrench| **Tightening torques**       |
  +======================+==================+
  | **Apply Loctite 9466**                  |
  +----------------------+------------------+
  | |TopSolarScrews|     | **0.4 Nm**       |
  +----------------------+------------------+

Antenna Stowing
---------------
.. container:: step

  .. rubric:: Antenna tie down
  .. image:: img/antenna-step-1.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Pass the Dyneema string under the thermal knife.
  * Start from from outer side (Y-) towards inner side (Y+).

.. container:: step

  .. image:: img/antenna-step-2.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Pass the string through the antenna string loop

.. container:: step

  .. image:: img/antenna-step-3.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Pass the string under the second thermal knife from inner side (Y+) towards outer side (Y-)

.. container:: step

  .. image:: img/antenna-step-4.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Finally pass it again through the antenna string mount

.. container:: step

  .. image:: img/antenna-step-5.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Cut the Dyneema string allowing for at least 10cm extra length

.. container:: step

  .. image:: img/antenna-step-6.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Make a square knot

  **Note:** A square knot is adequate to loosely keep the antenna from unfolding while it can be tightened a later step

.. container:: step

  .. image:: img/antenna-step-7.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Fold the open end of the antenna under the string loop end.

.. container:: step

  .. image:: img/antenna-step-8.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Make sure that the Dyneema string is at located at the center of each thermal knife
  * While holding the open end of the antenna in place, tighten the square knot
  * The antenna should push against the antenna release detector.
  * Verify proper position by ensuring that there is continuity on the test points using an DMM.

.. container:: step

  .. image:: img/antenna-step-9.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Verify that the Dyneema string is at the center of each thermal knife
  * Verify that the antenna is fully folded
  * Verify the continuity of the antenna release detector
  * Make 5 additional square knots

.. container:: step

  .. image:: img/antenna-step-10.jpeg
     :width: 40%
     :alt: alternate text
     :align: left

  * Add a drop of CA glue on the knots to secure them in place

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
