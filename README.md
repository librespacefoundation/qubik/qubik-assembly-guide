# QUBIK Assembly documentation #

This repository contains the source code of QUBIK assembly documentation.

The rendered documentation can be found [here](https://qubik.libre.space/projects/qubik-assembly-guide/).

## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202020-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
